<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{{$title ?? 'Perpustakaan'}}</title>

	@include('frontend.templates.partials.head')	

</head>
<body>
	<!-- Navigation -->
	@include('frontend.templates.partials.navbar')

  {{-- Content --}}
  <div class="container">
	    @yield('content')
  </div>
	
	@include('frontend.templates.partials.scripts')

	@include('frontend.templates.partials.toasts')

</body>
</html>