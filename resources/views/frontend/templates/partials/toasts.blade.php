@if(session('toasts'))
	<script>
       M.toast({html: '{{ session('toasts') }}', classes: 'rounded'})
	</script>
@endif