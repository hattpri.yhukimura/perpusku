<div class="navbar-fixed">
	<nav>
		<div class="nav-wrapper">
		    <a href="{{ route('homepage') }}" class="brand-logo">Perpusku</a>
		    <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
		    <ul class="right hide-on-med-and-down">

			    @guest
			    	<li><a href="{{ route('login') }}">Login</a></li>
		        	<li><a href="{{ route('register') }}">Register</a></li>	
				@else
					<ul id="dropdown1" class="dropdown-content">
					  <li>
						<a href="{{ route('homepage') }}">Koleksi Buku</a>
					  </li>	
					  <li>
						<a href="{{ route('home') }}">Buku dipinjam</a>
					  </li>					  
					  <li class="divider"></li>
					  <li>
					  	<a href="{{ route('logout') }}" onclick="event.preventDefault();
						   	 document.getElementById('logout-form').submit();">Logout</a>
					  </li>
					</ul>
					<ul class="right hide-on-med-and-down">					    
					  <li>
					    <a class="dropdown-trigger waves-effect" href="#!" data-target="dropdown1">{{ auth()->user()->name }}<i class="material-icons right">arrow_drop_down</i></a>
					  </li>
					</ul>	        
		        @endguest  
		    </ul>
		</div>
	</nav>	
</div>

<ul class="sidenav" id="mobile-demo">
   @guest
	   	<li><a href="{{ route('login') }}">Login</a></li>
	   	<li><a href="{{ route('register') }}">Register</a></li>	
	@else
		<li><a href="{{ route('homepage') }}">{{ auth()->user()->name }}</a></li>
   		<li class="divider" tabindex="-1"></li>
		<li>
	    	<a href="{{ route('logout') }}" onclick="event.preventDefault();
		   	 document.getElementById('logout-form').submit();">Logout</a>
		</li>		 	        
	@endguest  
</ul>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>	