@extends('frontend.templates.default')

@section('content')
  <h3>Koleksi Buku</h3>
    <blockquote>
      <p>Buku yang bisa dipinjam</p>
    </blockquote>
  
  <div class="row">
    @foreach($books as $book)
      @include('frontend.components.card-book', ['book' => $book])
    @endforeach
  </div> 

  {{ $books->render('vendor.pagination.materialize') }}


@endsection