@extends('frontend.templates.default')

@section('content')
  <h3>Koleksi buku yang di pinjam</h3>
  @foreach($books as $book) 

    <div class="col s12 m12 small">
        <div class="card horizontal hoverable">
          <div class="card-image">
            <img src="{{$book->getCover()}}">
          </div>
          <div class="card-stacked">
            <div class="card-content">
              <h4><b>{{ $book->title }}</b></h4>
              <blockquote>
                <p>{{ $book->description }}</p>
              </blockquote>
              <br>
              <i class="material-icons">person</i> : {{$book->author->name}}
              <br>             
              <i class="material-icons">date_range</i> : {{$book->pivot->created_at->diffForHumans()}}
            </div>                     
          </div>
        </div>
    </div>
  @endforeach
@endsection