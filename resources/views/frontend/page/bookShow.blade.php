@extends('frontend.templates.default')

@section('content')
  <h3>Koleksi Buku</h3> 
  <blockquote>
      <p>Rincian buku</p>
  </blockquote> 
  
  <div class="row">
    <div class="col s12 m12">
        <div class="card horizontal hoverable">
          <div class="card-image">
            <img src="{{$book->getCover()}}">
          </div>
          <div class="card-stacked">
            <div class="card-content">
              <h4><b>{{ $book->title }}</b></h4>
              <blockquote>
                <p>{{ $book->description }}</p>
              </blockquote>
              <br>
              <i class="material-icons">person</i> : {{$book->author->name}}
              <br>
              <i class="material-icons">book</i> : {{$book->qty}}
            </div>
            <div class="card-action">
                <form action="{{ route('book.borrow', $book) }}" method="POST">
                  @csrf
                  <button class="btn red accent-1 right waves-effect waves-light pulse">Pinjam Buku</button>
                </form>
            </div>
          </div>
        </div>
    </div>
  </div>

  <blockquote>
    <h5>Buku lainya dari penulis {{ $book->author->name }}</h5>
  </blockquote>

  <div class="row">
    @foreach($book->author->books->shuffle()->take(4) as $book)

     @component('frontend.components.card-book', ['book' => $book])
     
     @endcomponent

    @endforeach
  </div> 
@endsection