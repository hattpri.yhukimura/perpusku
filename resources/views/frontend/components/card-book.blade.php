<div class="col s6 m6 l6">
      <div class="card horizontal hoverable small" style="height: 200px;">
        <div class="card-image">
          <img src="{{$book->getCover()}}" height="200px">
        </div>
        <div class="card-stacked" >
          <div class="card-content">
            <h6 flow-text><b><a href="{{ route('book.show', $book) }}">{{ $book->title }}</a></b></h6>
            <p flow-text>{{ Str::limit($book->description, 70) }}</p>
          </div>
          <div class="card-action">
            <form action="{{ route('book.borrow', $book) }}" method="POST">
                  @csrf
                  <button class="btn blue accent-1 right waves-effect waves-light pulse flow-text">Pinjam Buku</button>
             </form>
          </div>
        </div>
      </div>
    </div>