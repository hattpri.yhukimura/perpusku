@extends('admin.template.default')

@section('content')
<div class="card">
    <div class="card-header">
         <h3 class="card-title">Edit Data Penulis</h3>         
    </div>
    <div class="card-body">
		<form action="{{ route('admin.book.update', [$book->id]) }}" method="POST" enctype="multipart/form-data">
		@csrf
		@method('PUT')
		  <div class="form-group row">
			    <label for="author_id" class="col-sm-2 col-form-label">Nama Penulis</label>
			    <div class="col-sm-10">
			      <select name="author_id" class="form-control custom-select @error('author_id') is-invalid @enderror select2" id="author_id">
			      <option value="">Pilih Nama Penulis</option>
			      	@foreach($name_author as $author)
			      	<option value="{{ $author->id }}"
			      		@if($author->id == $book->author_id)
							selected
			      		@endif 
			      		>{{ $author->name }}</option>
			      	@endforeach			      	
			      </select>
				    @error('author_id') 
				       <span class="help-block">{{ $message }}</span>	
				    @enderror
			    </div>
		  </div>
		  <div class="form-group row">
			    <label for="title" class="col-sm-2 col-form-label">Judul</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" placeholder="Masukan judul buku..." value="{{ old('title') ?? $book->title }}">
				    @error('title') 
				       <span class="help-block">{{ $message }}</span>	
				    @enderror
			    </div>
		  </div>
		  <div class="form-group row">
			    <label for="description" class="col-sm-2 col-form-label">Deskripsi</label>
			    <div class="col-sm-10">			    
			      <textarea name="description" class="form-control @error('description') is-invalid @enderror" id="description" rows="3">{{ old('description') ?? $book->description }}</textarea>
				    @error('description') 
				       <span class="help-block">{{ $message }}</span>	
				    @enderror
			    </div>
		  </div>
		  <div class="form-group row">
			    <label for="cover" class="col-sm-2 col-form-label">Sampul</label>
			    <div class="col-sm-10">			     	
					<input type="file" class="form-control-file @error('cover') is-invalid @enderror" id="cover" name="cover" value="{{ old('cover') ?? $book->cover }}">
		     		 @error('cover') 
		     	       <span class="help-block">{{ $message }}</span>	
		     	   	 @enderror					
			    </div>
		  </div>
		  <div class="form-group row">
			    <label for="qty" class="col-sm-2 col-form-label">Jumlah</label>
			    <div class="col-sm-10">
			      <input type="number" class="form-control @error('qty') is-invalid @enderror" id="qty" name="qty" min="0" value="{{ old('qty') ?? $book->qty }}">
				    @error('qty') 
				       <span class="help-block">{{ $message }}</span>	
				    @enderror
			    </div>
		  </div>

		  <div class="form-group row">
		    <div class="col-sm-12">
		    	<input type="submit" value="Ubah" class="btn btn-success btn-sm float-right">		    	
		    </div>
		  </div>		  
		</form>
    </div>
</div>
@endsection

@push('select2')
	<link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css') }}">
@endpush

@push('jquery')
	<script src="{{ asset('assets/plugins/select2/js/select2.full.min.js') }}"></script>
@endpush

@push('scripts')
	<script>
		$('.select2').select2();
	</script>
@endpush