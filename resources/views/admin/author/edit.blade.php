@extends('admin.template.default')

@section('content')
<div class="card">
    <div class="card-header">
         <h3 class="card-title">Edit Data Penulis</h3>         
    </div>
    <div class="card-body">
		<form action="{{ route('admin.author.update', [$author->id]) }}" method="POST">
		@csrf
		@method('PUT')
		  <div class="form-group row">
		    <label for="name" class="col-sm-2 col-form-label">Nama Penulis</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="Masukan Nama Penulis..." value="{{ old('name') ?? $author->name}}">
		      @error('name') 
			       <span class="help-block">{{ $message }}</span>	
			    @enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <div class="col-sm-12">
		    	<input type="submit" value="Ubah" class="btn btn-success btn-sm float-right">		    	
		    </div>
		  </div>		  
		</form>
    </div>
</div
@endsection