<a href="{{ route('admin.author.edit', $model) }}" class="btn btn-xs btn-warning">Edit</a>
<button href="{{ route('admin.author.destroy', $model) }}" class="btn btn-xs btn-danger" id="delete">Hapus</button>

<link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script>
    $('button#delete').on('click', function(e){
        e.preventDefault();
        var href = $(this).attr('href');

        const swalWithBootstrapButtons = Swal.mixin({
		  customClass: {
		    confirmButton: 'btn btn-sm btn-success',
		    cancelButton: 'btn btn-sm btn-danger'
		  },
		  buttonsStyling: false
		});

		Swal.fire({
		  title: 'Apakah kamu yakin?',		   	  
		  text: "Anda tidak akan dapat mengembalikannya!",
		  icon: 'warning',		  
		  showCancelButton: true,
		  confirmButtonText: 'Ya, Hapus ini!',
		  cancelButtonText: 'Tidak, Batalkan!', 
		}).then((result) => {
		  if (result.value) {
		  	document.getElementById('deleteForm').action = href;
            document.getElementById('deleteForm').submit();

		    swalWithBootstrapButtons.fire(
		      'Terhapus!',
		      'Data berhasil dihapus dari sistem.',
		      'success'
		    )
		  } else if (
		    result.dismiss === Swal.DismissReason.cancel
		  ) {
		    swalWithBootstrapButtons.fire(
		      'Dibatalkan',
		      'Data berhasil diamakan :)',
		      'error'
		    )
		  }
		});        
    });
</script>
