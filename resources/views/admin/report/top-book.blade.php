@extends('admin.template.default')

@section('content')

<div class="card">
    <div class="card-header">
         <h3 class="card-title">List Top Book</h3>        
    </div>
    <div class="card-body">
		<table class="table table-bordered table-hover text-sm table-sm table-responsive">
		 	<thead>
		 		<tr>
		 			<th>No</th>
		 			<th>Nama Penulis</th>
		 			<th>Judul</th>
		 			<th>Deskripsi</th>
		 			<th>Sampul</th>
		 			<th>Jumlah</th>
		 			<th>Total dipinjam</th>

		 		</tr>
		 	</thead>
		 	<tbody>
		 		<?php foreach ($books as $book): ?>
					<tr>
						<td>{{ $no++ }}</td>
						<td>{{ $book->author->name }}</td>
						<td>{{ $book->title }}</td>
						<td>{{ $book->description }}</td>
						<td>
							<img src="{{ $book->getCover() }}" alt="{{ $book->title }}" height="150px" class="img-fluid">
						</td>
						<td>{{ $book->qty }}</td>
						<td>{{ $book->borrowed_count }}</td>

					</tr>
		 		<?php endforeach ?>
		 	</tbody>
		 </table>
		 <br>
		 {{ $books->render() }}
    </div>
</div>

@endsection

