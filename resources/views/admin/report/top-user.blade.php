@extends('admin.template.default')

@section('content')

<div class="card">
    <div class="card-header">
         <h3 class="card-title">List Top User</h3>        
    </div>
    <div class="card-body col-sm-12 col-md-12">
		<table class="table table-bordered table-hover text-sm table-sm table-responsive">
		 	<thead>
		 		<tr>
		 			<th>No</th>
		 			<th>Nama</th>
		 			<th>Email</th>		 			
		 			<th>Total Meminjam</th>
		 			<th>Bergabung</th>
		 		</tr>
		 	</thead>
		 	<tbody>
		 		<?php foreach ($users as $user): ?>
					<tr>
						<td>{{ $no++ }}</td>
						<td>{{ $user->name }}</td>
						<td>{{ $user->email }}</td>	
						<td>{{ $user->borrow_count }}</td>	
						<td>{{ $user->created_at->diffForHumans() }}</td>				

					</tr>
		 		<?php endforeach ?>
		 	</tbody>
		 </table>
		 <br>
		 {{ $users->render() }}
    </div>
</div>

@endsection

