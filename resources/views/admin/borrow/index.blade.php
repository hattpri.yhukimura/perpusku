@extends('admin.template.default')

@section('content')

<div class="card">
    <div class="card-header">
         <h3 class="card-title">Data Peminjaman</h3>
         <a href="{{ route('admin.author.create') }}" class="btn btn-primary btn-sm float-right">Tambah Penulis</a>
    </div>
    <div class="card-body">

		

		<table id="authorData" class="table table-bordered table-hover text-sm table-sm">
		 	<thead>
		 		<tr>
		 			<th>No</th>
		 			<th>Nama Peminjam</th>
		 			<th>Buku Dipinjam</th>
		 			<th>Tanggal Peminjaman</th>
		 			<th>Waktu Pinjam</th>
		 			<th>Aksi</th>
		 		</tr>
		 	</thead>
		 	<tbody>
		 		<tr></tr>
		 	</tbody>
		 </table>
    </div>
</div>
<form action="" method="post" id="borrowForm">
    @csrf
    @method("PUT")
    <input type="submit" value="Hapus" style="display: none">
</form>
@endsection

@push('styles')
	 <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
	 <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}"> 
@endpush


@push('jquery')
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/bootstrap-notify/bootstrap-notify.min.js') }}"></script>	
@endpush

@push('scripts')
	@include('admin.template.partials.alerts')
<script>
	$(function () {
		$('#authorData').DataTable({
		 		processing: true,
                serverSide: true,
                ajax: '{{ route('admin.borrow.data') }}',
                columns: [
                	{ data: 'DT_RowIndex', orderable: false, searchable : false},                
                    { data: 'user'} ,
                    { data: 'book'},
                    { data: 'created_at'},
                    { data: 'range_time'},
                    { data: 'action'}              
                ]                         
		});
	});
</script>
@endpush