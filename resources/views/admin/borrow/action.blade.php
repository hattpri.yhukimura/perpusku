<button href="{{ route('admin.borrow.update', $model) }}" class="btn btn-xs btn-info" id="borrow">Dikembalikan</button>

<link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script>
    $('button#borrow').on('click', function(e){
        e.preventDefault();
        var href = $(this).attr('href');

        const swalWithBootstrapButtons = Swal.mixin({
		  customClass: {
		    confirmButton: 'btn btn-sm btn-success',
		    cancelButton: 'btn btn-sm btn-danger'
		  },
		  buttonsStyling: false
		});

		Swal.fire({
		  title: 'Apakah kamu yakin?',		   	  
		  text: "Pastikan buku yang dikembalikan!!",
		  icon: 'warning',		  
		  showCancelButton: true,
		  confirmButtonText: 'Ya, Kembalikan!',
		  cancelButtonText: 'Tidak, Batalkan!', 
		}).then((result) => {
		  if (result.value) {
		  	document.getElementById('borrowForm').action = href;
            document.getElementById('borrowForm').submit();

		    swalWithBootstrapButtons.fire(
		      'Berhasil!',
		      'Data buku berhasil dikembalikan.',
		      'success'
		    )
		  } else if (
		    result.dismiss === Swal.DismissReason.cancel
		  ) {
		    swalWithBootstrapButtons.fire(
		      'Dibatalkan',
		      'Data buku berhasil diamankan :)',
		      'error'
		    )
		  }
		});        
    });
</script>
