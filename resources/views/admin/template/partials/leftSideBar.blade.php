 <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('admin.dashboard') }}" class="brand-link">     
      <p class="brand-text font-weight-light text-center"><b>PerPus</b></p>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 ml-3">       
        <div class="info">
          <a href="{{ route('admin.dashboard') }}" class="d-block" style="text-align: center;">{{ auth()->user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
         
          <li class="nav-item has-treeview">
            <a href="{{ route('admin.author.index') }}" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>Penulis</p>
            </a>            
          </li>

          <li class="nav-item has-treeview">
            <a href="{{ route('admin.book.index') }}" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>Buku</p>
            </a>            
          </li>

          <li class="nav-item has-treeview">
            <a href="{{{ route('admin.borrow.index') }}}" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>Buku Dipinjam</p>
            </a>            
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Laporan
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{ route('admin.report.top-book') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Buku Teratas</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('admin.report.top-user') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>User Teratas</p>
                  </a>
                </li>
              </ul>                  
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>User</p>
            </a>            
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>