<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Administrator Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('dashboard');

// Route::get('/author', 'AuthorController@index')->name('author');
// Route::get('/author/create', 'AuthorController@create')->name('author.create');
// Route::post('/author', 'AuthorController@store')->name('author.store');
// Route::get('/author/{author}/edit', 'AuthorController@edit')->name('author.edit');
// Route::put('/author/{author}', 'AuthorController@update')->name('author.update');
// Route::delete('/author/{author}', 'AuthorController@destroy')->name('author.destroy');

//Datatables
Route::get('/author/data', 'DataCenterController@authorDatatable')->name('author.data');
Route::get('/book/data', 'DataCenterController@bookDatatable')->name('book.data');
Route::get('/borrow/data', 'DataCenterController@borrowDatatable')->name('borrow.data');
//-----------------------------------------------------------------------------------------


Route::resource('/author', 'AuthorController');
Route::resource('/book', 'BookController');

Route::get('/borrow', 'BorrowController@index')->name('borrow.index');
Route::put('/borrow/{borrow}', 'BorrowController@update')->name('borrow.update');

Route::get('/report/top-book', 'ReportController@topBook')->name('report.top-book');
Route::get('/report/top-user', 'ReportController@topUser')->name('report.top-user');