<?php

use Illuminate\Database\Seeder;
use App\User;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $user = User::create([
       	'name' 		           => 'Administrator',
       	'email' 	           => 'admin@test.com',
        'email_verified_at'  => now(),
       	'password'	         => Hash::make('12345678'),
       	
       ]);

       $user->assignRole('admin');
    }
}
