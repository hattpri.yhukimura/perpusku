<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $user = User::create([
        'name'               => 'User',
        'email'              => 'user@test.com',
        'email_verified_at'  => now(),
        'password'           => Hash::make('12345678'),
        
       ]);

       $user->assignRole('user');
    }
}
