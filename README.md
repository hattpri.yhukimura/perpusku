## About PerpusKu

Lengkap dengan menggunakan :

- [Laravel Debugbar](https://github.com/barryvdh/laravel-debugbar).
- [Faker](https://github.com/fzaninotto/Faker#fakerproviderlorem).
- [Datatables Yajra](https://github.com/yajra/laravel-datatables).
- [SweetAlert](https://sweetalert2.github.io).
- [Bootstrap - notify](http://bootstrap-notify.remabledesigns.com).
- [Breadcrumbs](https://github.com/davejamesmiller/laravel-breadcrumbs).
- [Image, Placeholder](https://placeholder.com).
- [Materialize - ui](https://materializecss.com).