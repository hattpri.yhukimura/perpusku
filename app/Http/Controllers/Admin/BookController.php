<?php

namespace App\Http\Controllers\Admin;

use App\Book;
use App\Author;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.book.index', [
            'title' => 'Data Buku'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {       
       $author = Author::orderBy('name', 'asc')->get();

       return view('admin.book.create', [
            'title' => 'Tambah Buku',
            'name_author' => $author
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'author_id'     => 'required',
            'title'         => 'required',
            'description'   => 'required|min:10',
            'cover'         => 'file|image',
            'qty'           => 'required|numeric'
        ]);

        $cover = null;

        if ($request->hasFile('cover')) {
            $cover = $request->file('cover')->store('assets/covers');
        }

        Book::create([
            'author_id'     => $request->author_id,
            'title'         => $request->title,
            'description'   => $request->description,
            'cover'         => $cover,
            'qty'           => $request->qty
        ]);

        return redirect()->route('admin.book.index')
            ->withSuccess('Data buku berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        $author = Author::orderBy('name', 'asc')->get();

        return view('admin.book.edit', [
            'book' => $book,
            'name_author' => $author,
            'title' => 'Edit Data Buku'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $this->validate($request, [
            'author_id'     => 'required',
            'title'         => 'required',
            'description'   => 'required|min:10',
            'cover'         => 'file|image',
            'qty'           => 'required|numeric'
        ]);

        $cover = $book->cover;

        if ($request->hasFile('cover')) {
            storage::delete($book->cover);
            $cover = $request->file('cover')->store('assets/covers');
        }

        $book->update([
            'author_id'     => $request->author_id,
            'title'         => $request->title,
            'description'   => $request->description,
            'cover'         => $cover,
            'qty'           => $request->qty
        ]);

        return redirect()->route('admin.book.index')
            ->withInfo('Data buku berhasil diperbaharui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        storage::delete($book->cover);
        $book->delete();

       return redirect()->route('admin.book.index')
       ->with('danger', 'Data berhasil dihapus!');
    }
}
