<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Book;
use App\User;

class ReportController extends Controller
{
    public function topBook()
    {
    	$books = Book::with('author')
    			->withCount('borrowed')
    			->orderBy('borrowed_count', 'desc')
    			->paginate(10);

    	$page = 1;

    	if (request()->has('page')) {
    		$page = request('page');
    	}

    	$no = (10 * $page) - (10 - 1);

    	return view('admin.report.top-book', [
    		'no'	=> $no,
    		'books' => $books,
    		'title' => 'Buku Teratas'
    	]);
    }

    public function topUser()
    {
    	$users = User::
    			withCount('borrow')
    			->orderBy('borrow_count', 'desc')
    			->paginate(10);

    	if (request()->has('page')) {
    		$page = request('page');
    	}

    	$no = (10 * $page) - (10 - 1);

    	return view('admin.report.top-user', [
    		'no'	=> $no,
    		'users' => $users,
    		'title' => 'User Teratas'
    	]);
    }
}
