<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Author;
use App\Book;
use App\BorrowHistory;
use Carbon\Carbon;

class DataCenterController extends Controller
{
   public function authorDatatable()
   {

   	$query = Author::orderBy('name', 'asc');
   	return datatables()->of($query)
   			 ->addIndexColumn()
   		  	 ->addColumn('action', 'admin.author.action')
         	 ->rawColumns(['action'])
   			 ->toJson();
   }

   public function bookDatatable()
   {
   	$query = Book::with('author')->orderBy('title', 'asc')->get();
    // $query->load('author');
   	return datatables()->of($query)
   			 ->addIndexColumn()
   			 ->addColumn('author', function(Book $model){
   			 	return $model->author->name;
   			 })
   			 ->editColumn('cover', function(Book $model){
   			 	return '<img src="'. $model->getCover() .'" alt="cover" height="100px" class="img-fluid">';
   			 })   			
	   		->addColumn('action', 'admin.book.action')
	         ->rawColumns(['cover','action'])
   			->toJson();
   }

   public function borrowDatatable()
   {
      $query = BorrowHistory::IsBorrowed()->latest()->get();
      $query->load('user', 'book');
      return datatables()->of($query)
             ->addIndexColumn()
             ->addColumn('user', function(BorrowHistory $model){
               return $model->user->name;
             }) 
             ->addColumn('book', function(BorrowHistory $model){
               return $model->book->title;
             })
             ->addColumn('range_time', function(BorrowHistory $model){
               return $model->created_at->diffForHumans(['short' => true]);
             })
              ->editColumn('created_at', function(BorrowHistory $model){
               return  $model->created_at->toFormattedDateString();
             })  
           /* ->editColumn('return_at', function(BorrowHistory $model){
                if ($model->return_at === null) {
                  $returned = $model->return_at;
                }else{
                  $returned = Carbon::create($model->return_at)->toFormattedDateString();
                }
                return  $returned;
             })    */                       
            ->addColumn('action', 'admin.borrow.action')
            ->rawColumns(['action'])
            ->toJson();
   }
}
