<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\BorrowHistory;
use App\Book;
use Carbon\Carbon;

class BorrowController extends Controller
{
    public function index()
    {
    	return view('admin.borrow.index',[
    		'title' => 'Data buku yang dipinjam'
    	]);
    }

    public function update(Request $request, BorrowHistory $borrow)
    {
    	$borrow->update([
    		'return_at' => Carbon::now(),
    		'admin_id'	=> auth()->user()->id
    	]);

    	$borrow->book->increment('qty');

    	return redirect()->route('admin.borrow.index')->withInfo('Data buku berhasil dikembalikan');
    }
}
