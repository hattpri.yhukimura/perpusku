<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Book;
use App\BorrowHistory;

class FrontendController extends Controller
{
    public function homePage()
    {
    	$books = Book::paginate(6);

    	return view('frontend.page.home', [
    		'books' => $books
    	]);
    }

    public function showBook(Book $book)
    {
    	return view('frontend.page.bookShow', [
    		'book' => $book,
            'title' => 'Detail buku'
    	]);
    }

    public function borrow(Book $book)
    {
    	$user = auth()->user();

        if ($user->borrow()->IsStillBorrow($book->id)) {
           return redirect()->route('homepage')->with('toasts', 'Kamu sudah meminjam buku ini dengan judul : '.$book->title);
        }
    	$user->borrow()->attach($book);
        $user->borrow()->decrement('qty');

    	return redirect()->route('homepage')->with('toasts', 'Buku '.$book->title.' berhasil dipinjam');
    }
}
